
Basé sur [le fichier myframa-master-....zip  du 15 déc. 2016 17:49:23](https://framagit.org/framasoft/myframa/ "myframa Project ID: 8356")

 -  Et sur [le README.md](https://framagit.org/framasoft/myframa/-/blob/master/README.md)
 -  ET sur [la Version v0.7.0 released this on 14 May 2016](https://github.com/shaarli/Shaarli/releases/tag/v0.7.0)
 -  [MyFrama (Shaarli)](https://my.framasoft.org/ "Alternatives à MyFrama (Shaarli)")
 -  [Cultiver son jardin](https://framacloud.org/fr/ "Framacloud – du libre dans les nuages")


**LES MODIFICATIONS** par rapport aux fichiers originaux

Le dossier shaarli sera prévu pour installation directe pour un seul utilisateur.

 1 Garder le dossier shaarli

 2 Ajouter les dossiers /cache /data /pagecache /tmp avec .htaccess

3 Supprimer les fichiers du dossier /tpl SAUF includes.html (pour la page du premier lancement)

 3a Ajouter les fichiers index.html + .htaccess dans  /tpl/ et  /tpl/frama

 4 Supprimer les fichiers du dossier /doc et ajouter par sécurité fichier index.html + .htaccess 

 4b Modifier le lien /tpl/frama/page.footer.html pour lien direct vers la doc (https://github.com/shaarli/Shaarli/wiki) et ajout lien vers Framasoft/myframa (https://framagit.org/framasoft/myframa/)

 5 Modifier le fichier index.php à la racine :

//-$GLOBALS['config']['RAINTPL_TPL'] = 'tpl/'; 		//supprimé pour myframa line 62
$GLOBALS['config']['RAINTPL_TPL'] = 'tpl/frama/'; 	//remplacé pour myframa
//+ Pour myframa line 118
$GLOBALS['config']['ENABLED_PLUGINS'] = array (
  0 => 'i18n',
  1 => 'tags_advanced',
  2 => 'qrcode',
);
//$GLOBALS['config']['ENABLED_PLUGINS'] = array('qrcode'); // supprimé pour myframa

5a Modifier le fichier /tpl/frama/install.html avec {include="includes"} en {include="../includes"} 
Si réinstallation de Shaarli en stand alone pour afficher les CSS

 6 Le fichier /plugin/tags_advanced/example.php doit être déplacé dans /data/ renommé en tags_advanced.php, accessible en écriture

 7 Correction suivant Issue #17  https://framagit.org/framasoft/myframa/issues/17
  Fichier /tpl/frama/page.header.html {if="$plugins_header._PAGE_ == linklist"}
  modifié en {if="$plugins_header._PAGE_ == 'linklist'"}

 7a Fichier /tpl/frama/changetag.html  {'Old tag'|t} est Non traduit, modifié en {'Old tag:'|t} et supprimé le : mal placé

 8 traduction en français du fichier /tpl/frama/install.html

 9 Correction des erreurs HTML (https://validator.w3.org/nu/) dans
- /tpl/frama/ 404.html, addlink.html, changepassword.html, changetag.html, configure.html, daily.html, export.html, import.html, includes.html, install.html, linklist.html, page.footer.html, page.header.html, page.html, picwall.html, pluginsadmin.html, tagcloud.html, tools.html, 
- /plugins/tags_advanced/tools.html
- /plugins/tags_advanced/tags_advanced.php
- /plugins/qrcode/qrcode.html
  Issue #18 https://framagit.org/framasoft/myframa/issues/18

 10 Dans index.php 
- Traduction en français des messages en anglais
- Correction (suppression) de json_encode dans les 2 lignes  echo '... .json_encode($filename) ...  pour afficher correctement les messages d'importation de fichier

 11 Corrections diverses
- Ajout d'un message alerte si suppression d'un lien dans la "Liste des liens" linklist.html 
- Pour afficher la liste des tags dans la page Journal (daily.html) modification du lien ?addtag={$value|urlencode remplacement de addtag par searchtags
- Ajout target="_blank" sur URL des liens en haut et en bas pour afficher dans un autre onglet

 12 Corrections Commit a5c6b5cb myframa
- function_exists(t) par function_exists('t') plugins/myframa/myframa.js.php plugins/recovery/recovery.js.php, recovery/recovery.php 

 13 v0.7.1 basée sur https://github.com/shaarli/Shaarli/releases/tag/v0.7.1
- Markdown plugin: escape HTML entities by default

 14 v0.7.2 basée sur https://github.com/shaarli/Shaarli/commit/28df9fa4f7d901632e7fd6e23b6d7dd345f1f6e7#diff-92386114a829d5593d8f0bf831062c96
 - Deprecated: idn_to_ascii(): INTL_IDNA_VARIANT_2003 is deprecated /application/Url.php on line 249
	voir https://wiki.php.net/rfc/deprecate-and-remove-intl_idna_variant_2003
 - Correction CSS sur /plugins/tags_advanced/tags_advanced.php
 - Correction JS sur /tpl/frama/page.footer.html suppression de type="text/javascript" dans <script...
 - Correction HTML sur /tpl/loginform.html + /tpl/editlink.html 

 15 v0.7.2-b - 21/10/2019
 - Alerte PHP 7.4 Deprecated: Function get_magic_quotes_gpc() is deprecated if (get_magic_quotes_gpc())   [index.php] correction du 20/10/19
 - Version PHP mini checkPHPVersion('5.3' => checkPHPVersion('5.6'  [index.php]  correction du 21/10/19
 - Traduction Shaarli requires at least PHP ... [/application/ApplicationUtils.php]
 - Undefined index link['thumbnail']  [index.php]  correction du 21/10/19
 - Correction ban_loginFailed() [index.php]  correction du 21/10/19
 
 16 v0.7.2-c - 05/01/2021
- PHP 8.0 tests
- Typo sur qrcode.html
- Ajout d'un fichier index.html sur tous les dossiers


 17 v0.7.2-d - 28/12/2022
- Dernière version compatible PHP 5.6.x, fonctionne aussi en PHP 7.x et 8.0.x 
- Correction "rejected by sanitizer" par remplacement de la version 4.7.0 du fichier /tpl/frama/inc/font-awesome/fonts/fontawesome-webfont.woff2  (Firefox Console)
- Correction des fichiers CSS pour validation CSS du W3C : font-awesome.min.css (Version 4.6.1->4.7.0), /bootstrap/css/bootstrap.min.css et bootstrap.css et /inc/frama.css
- Correction pour HTML 5, modification des balises fermantes /> par > en conformité  https://validator.w3.org/nu/ (Nu Html Checker)
  - modifications des différents fichiers non conformes 
  - modification du fichier /application/Utils.php avec la fonction function format_description(...) par ajout de , false pour remplacer les < br /> par des < br > dans le texte de la Description des Liste des liens.
- Modification des tags du fichier /data/tags_advanced.php (remplacement des Framaxxx par Tag_X)
- Ajout des balises title sur la zone Tags (modifier les tags et Afficher tous les tags)
- Nota pour modifier les icônes du fichier /data/tags_advanced.php consuler la page https://fontawesome.com/v4/icons/ et pour les couleurs le choix (fc_g7','fc_g9','fc_b8','fc_b9','fc_v7','fc_v9', 'fc_o8','fc_r5','fc_m5','fc_j6','fc_f5) fait référence au fichier /tpl/frama/inc/frama.css
   
 
 18 v0.7.2-e 29/12/2022
- Correction pour PHP 8.1
- Version PHP 8.0.x mini, fonctionne aussi en PHP 8.1.x et 8.2.x
 + Modification de la version pour PHP 8.0 mini sur index.php ( ApplicationUtils::checkPHPVersion)
Modification des fichiers
 +/application/Utils.php avec ajout de la class.cms_strftime_bohwaz-php-8.1-strftime.php modifiée pour remplacer strftime obsolète en PHP 8.1  ( function strftime => function mystrftime )
 + Modification de strftime par mystrftime pour /tpl/frama/daily.html, /tpl/frama/dailyrss.html, /tpl/frama/linklist.html
 + /application/LinkDB.php (ajout de 10 #[\ReturnTypeWillChange]  pour Deprecated: Return type) 
 + /inc/rain.tpl.class.php (correction de $params  par  $params ?? ''  en ligne 731 et 834 pour Deprecated: str_replace()) et correction /> en ligne 922 
 +  /application/Utils.php correction affichage Journal  Passing null to parameter #1 ($string) ligne 71
 
 19 v0.7.2-e 26/01/2023
- Correction par ajout de "ouvrir le lien dans un autre onglet" du fichier linklist.html en ligne 72 et 114
- Correction et traduction FR des fichiers .meta dans le dossier /plugins/tags_advanced.meta, /qrcode.meta, /addlink_toolbar.meta, /archiveorg.meta, /demo_plugin.meta, /framanav.meta, /markdown.meta, /playvideos.meta
- Correction et traduction FR des fichiers dans le dossier /plugins/addlink_toolbar/addlink_toolbar.html, /archiveorg/archiveorg.html
- Suppression du dossier /plugins/myframa/ le service my.framasoft.org est supprimé
- Suppression des dossiers /plugins/readityourself, /recovery, /wallabag
- Suppression de la case [M’informer s’il existe une nouvelle version] dans /tpl/frama/configure.html
  
---
**INSTALLATION et UTILISATION**

Installation : Téléchargez les fichiers. Décompressez l’archive et copiez le dossier shaarli sur votre espace web.
- vous pouvez aussi si besoin renommer ce dossier shaarli.
- Les dossiers suivant : cache/, data/, pagecache/ et tmp/ doivent être en lecture/écriture.

Configuration : Pour configurer le logiciel, se rendre sur http://votre-site.xxx/shaarli. 
- Entrer les informations Utilisateur, Mot de passe, Titre de la page et sauvegarder.

Utilisation : Connectez vous et entrez vos liens.


---
### License
Shaarli is [Free Software](http://en.wikipedia.org/wiki/Free_software). See [COPYING](https://github.com/shaarli/Shaarli/blob/master/COPYING) for a detail of the contributors and licenses for each individual component.
