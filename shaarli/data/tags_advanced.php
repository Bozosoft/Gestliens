<?php
/**
* Nota pour modifier les icônes du fichier /data/tags_advanced.php consulter la page https://fontawesome.com/v4/icons/ 
* Et pour les couleurs le choix (fc_g7','fc_g9','fc_b8','fc_b9','fc_v7','fc_v9', 'fc_o8','fc_r5','fc_m5','fc_j6','fc_f5) 
* fait référence au fichier /tpl/frama/inc/frama.css
*/
    $taData = array(
        0 => array('dégooglisons', 'fa-shield fc_o5', 'degooglisons-internet.org', true, false),
        1 => array('projet-top-secret', 'fa-clock-o fc_j6', '', true, true),
        2 => array('Software', 'fa-universal-access fc_v7', '', true, false),
        3 => array('Tag_2', 'fa-linux fc_v7', '', true, false),
        4 => array('Tag_3', 'fa-file-o fc_v7', '', true, false),
        5 => array('Tag_4', 'fa-list-ul fc_v7', '', true, false),
        6 => array('Tag_5', 'fa-scissors fc_v7', '', true, false),
        7 => array('Tag_6', 'fa-sitemap fa-rotate-270 fc_v7', '', true, false),
        8 => array('Tag_7', 'fa-object-group fc_v7', '', true, false),
        9 => array('Tag_8', 'fa-calendar fc_v7', '', true, false),
        10 => array('Git', 'fa-gitlab fc_v9', 'framagit.org', true, false),
    );
?>
