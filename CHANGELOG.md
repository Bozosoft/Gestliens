
# Change Log

## v0.7.2-f 26/01/2023
- Correction par ajout de "ouvrir le lien dans un autre onglet" du fichier linklist.html
- Correction et traduction FR des fichier .meta dans le dossier /plugins/
- Suppression des dossiers /plugins/ de myframa, readityourself, recovery,wallabag
	- lire le fichier README.md (19) 

## v0.7.2-e 29/12/2022
- Correction pour PHP 8.1.x
- Version PHP 8.0.x mini, fonctionne aussi en PHP 8.1.x et 8.2.x
- Ajout de la class.cms_strftime_bohwaz-php-8.1-strftime pour remplacer strftime obsolète en PHP 8.1.x
- Modification des fichiers daily.html, dailyrss.html, /linklist.html pour remplacer strftime obsolète en PHP 8.1.x
- Modification de /LinkDB.php  pour Deprecated: Return type en PHP 8.1.x
- Modification de rain.tpl.class.php pour Deprecated: str_replace() PHP 8.1.x
- Correction affichage Journal  Passing null to parameter #1 ($string) ligne 71 PHP 8.1.x  /application/Utils.php
	- lire le fichier README.md (18) 


## v0.7.2-d 28/12/2022
### Changed
- Dernière version compatible PHP 5.6.x, fonctionne aussi en PHP 7.x et 8.0.x
- Correction des fichiers CSS 
- Correction pour HTML 5, modification des balises fermantes /> par >
- Modification des noms des Tags
	- lire le fichier README.md (17)

## v0.7.2-c 05/01/2021
### Changed
- PHP 8.0 tests
- Typo sur qrcode.html
- Ajout d'un fichier index.html sur tous les dossiers
	- lire le fichier README.md (16)

## v0.7.2-b 21/10/2019 
### Fixed
- PHP 7.4 Correction Alerte Deprecated: Function get_magic_quotes_gpc()
- Version PHP mini 5.6
- Correction Undefined index link['thumbnail'] 
- Correction ban_loginFailed()
	- lire le fichier README.md (15)

## v0.7.2-a 10/10/2019
### Typo correction index.php

## v0.7.2  - 17/11/2018
### Fixed
- PHP 7.2.x Deprecated: idn_to_ascii(): INTL_IDNA_VARIANT_2003 is deprecated
- Diverses corrections HTML CSS et JS
    - lire le fichier README.md (14)


## v0.7.1 - 27/04/2017
### Security
- Markdown plugin: escape HTML entities by default 
    - lire le fichier README.md ( 13)

## v0.7.0-b - 18/03/2017
### Fixed
- Corections divers
    - lire le fichier README.md ( 12)

## v0.7.0-a - 04/02/2017
### Added
- Ajout fichier README.md
- Ajout fichier CHANGELOG.md pour l'évolution de modifications

### Changed
- Ajout target="_blank" sur URL des liens en haut et en bas pour afficher dans un autre onglet
- for fix PHP Notice: Use of undefined constant t - assumed 't' in myframa/shaarli/plugins/tags_advanced/tags_advanced.php on line 4 (https://framagit.org/framasoft/myframa/merge_requests/3)
- Modifications du code original pour mise en conformatité HTML
    - lire le fichier README.md ( 1 à 11)

### Fixed
- Corections divers
    - lire le fichier README.md ( 1 à 11)


## [Basé sur v0.7.0] - 2016-05-14 
- (https://github.com/shaarli/Shaarli/tree/stable)
